import React from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Form, Button, Stack } from 'react-bootstrap'

export default function SearchBox() {
  const navigate = useNavigate()
  const [keyword, setKeyword] = useState('')
  const submitHandler = (e) => {
    e.preventDefault()
    if (setKeyword.trim()) {
      navigate(`/search/${keyword}`)
    } else {
      navigate('/')
    }
  }
  return (
    <Stack direction='horizontal' gap={3}>
      <Form id='search-input' onSubmit={submitHandler}>
        <Form.Control
          type='text'
          name='q'
          onChange={(e) => setKeyword(e.target.value)}
          placeholder='Search product...'
          className='me-auto'
        />
      </Form>
      <Button
        form='search-input'
        type='submit'
        variant='outline-success'
        className='p-2'
      >
        Search
      </Button>
    </Stack>
  )
}
